## BUilding

To build this platform for pantavisor follow the following steps on a machine matching
the architecture you want to build for.

For example:

```
$ make export-squash
docker build  --no-cache -t asac/pvplatform-alpine-router .
Sending build context to Docker daemon  67.58kB
Step 1/5 : FROM alpine
 ---> e21c333399e0
Step 2/5 : RUN apk update; apk add openrc iptables busybox-initscripts busybox-extras dropbear;         rc-update add networking sysinit default;   rc-update add udhcpd default;   rc-update add dropbear default;         rc-update add syslog sysinit default;   rc-update add modules boot sysinit default;         touch /etc/iptables/rules-save;         echo root:pantacor | chpasswd
 ---> Running in e2ec8e83c461
fetch http://dl-cdn.alpinelinux.org/alpine/v3.7/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.7/community/x86_64/APKINDEX.tar.gz
v3.7.0-177-g78ce279c75 [http://dl-cdn.alpinelinux.org/alpine/v3.7/main]
v3.7.0-173-g53f4e4e2a7 [http://dl-cdn.alpinelinux.org/alpine/v3.7/community]
OK: 9051 distinct packages available
(1/9) Installing busybox-extras (1.27.2-r8)
Executing busybox-extras-1.27.2-r8.post-install
(2/9) Installing openrc (0.24.1-r4)
Executing openrc-0.24.1-r4.post-install
(3/9) Installing busybox-initscripts (3.1-r2)
Executing busybox-initscripts-3.1-r2.post-install
(4/9) Installing dropbear (2017.75-r1)
(5/9) Installing dropbear-openrc (2017.75-r1)
(6/9) Installing libmnl (1.0.4-r0)
(7/9) Installing jansson (2.10-r0)
(8/9) Installing libnftnl-libs (1.0.8-r1)
(9/9) Installing iptables (1.6.1-r1)
Executing busybox-1.27.2-r6.trigger
OK: 9 MiB in 20 packages
 * service networking added to runlevel sysinit
 * service networking added to runlevel default
 * service udhcpd added to runlevel default
 * service dropbear added to runlevel default
 * service syslog added to runlevel sysinit
 * service syslog added to runlevel default
 * service modules added to runlevel boot
 * service modules added to runlevel sysinit
 * service modules added to runlevel default
chpasswd: password for 'root' changed
Removing intermediate container e2ec8e83c461
 ---> 0f67ac1da15a
Step 3/5 : ADD modules /etc/
 ---> 041c7e5f18ce
Step 4/5 : ADD interfaces /etc/network/
 ---> 41f24fe7006f
Step 5/5 : ADD udhcpd.conf /etc/
 ---> f2b05f274af8
Successfully built f2b05f274af8
Successfully tagged asac/pvplatform-alpine-router:latest
Error: No such container: pvplatform-alpine-router-build
Exported available at: /tmp/pvplatform-alpine-router-yKIgn.tgz
rm -rf /tmp/pvplatform-alpine-router-kZUWl.raw; mkdir -p /tmp/pvplatform-alpine-router-kZUWl.raw
tar -C /tmp/pvplatform-alpine-router-kZUWl.raw -xf /tmp/pvplatform-alpine-router-yKIgn.tgz
mksquashfs /tmp/pvplatform-alpine-router-kZUWl.raw/ /tmp/pvplatform-alpine-router-H9I7f.squashfs -comp xz
Parallel mksquashfs: Using 4 processors
Creating 4.0 filesystem on /tmp/pvplatform-alpine-router-H9I7f.squashfs, block size 131072.
[===================================================================================================================-] 403/403 100%

Exportable Squashfs 4.0 filesystem, xz compressed, data block size 131072
        compressed data, compressed metadata, compressed fragments, compressed xattrs
        duplicates are removed
Filesystem size 3566.74 Kbytes (3.48 Mbytes)
        40.33% of uncompressed filesystem size (8843.16 Kbytes)
Inode table size 5360 bytes (5.23 Kbytes)
        18.09% of uncompressed inode table size (29637 bytes)
Directory table size 7644 bytes (7.46 Kbytes)
        49.40% of uncompressed directory table size (15473 bytes)
Number of duplicate files found 54
Number of inodes 852
Number of files 372
Number of fragments 22
Number of symbolic links  368
Number of device nodes 0
Number of fifo nodes 0
Number of socket nodes 0
Number of directories 112
Number of ids (unique uids + gids) 1
Number of uids 1
        asac (1000)
Number of gids 1
        asac (1000)
```

If you screen the log output above, you will see that this produced a squashfs /tmp/pvplatform-alpine-router-H9I7f.squashfs.

# Send squashfs as platform update to your device on pantahub.com

The squashfs from the step above can be delivered as OTA network to your device through https://www.pantahub.com

As step 1 you need to clone your device:

```
$ pvr clone https://pvr.pantahub.com/yournick/yourdevice
...

```

This produces a folder "yourdevice" with all firmware artifacts.

If you run an alpine based system yet, you can simply replace the
squashfs of the current root platform with your new squashfs and
use pvr to commit that change and post it to your device through pantahub.com:

```
$ cp -f /tmp/pvplatform-alpine-router-H9I7f.squashfs yourdevice/bpi-root.squashfs
```

And now commit and post the changes to your device

```
$ pvr commit .
$ pvr post --commit-msg "new alpine platform that i built myself"
```

