#!/bin/bash

set -x
set -e

#For GPT type disk size will probably be more than
#image size and parted will report GPT backup is
#corrupted. Fix that first.

#Needs a message to check for as argument
function check_gpt_for_error {
	__has_error=0
	if [ "$1"x != x ]
	then
		parted_error_file=`mktemp -p "$TMPDIR" parted.XXXXXX`
		parted -s $device print 2>${parted_error_file} 1>/dev/null
		if [ `grep -n "$1" ${parted_error_file} | wc -l` -gt 0 ]
		then
			__has_error=1
		fi
		rm ${parted_error_file}
	fi
	echo "${__has_error}"
}

fstype=
partnum=
is_last_part=1
if [ "$1"x = x ]
then
	echo "Usage $0 <block_device>"
	exit -1
fi
device="$1"
realdevice="$1"

if [ ! -b "$device" ]
then
	echo "Device $device doesn't exist or is not a block device. Exiting"
	exit -1
fi

#If the device is a partition,
#get it's root device name
devname=`basename $device`
if [ "$devname" = "." ] || [ "$devname" = ".." ] || [ "$devname"x = x ]
then
	echo "Need a block device, provided $device"
	exit -1
fi

this_partnum=
if [ -f /sys/class/block/$devname/partition ]
then
	__link="`readlink -f /sys/class/block/$devname/..`"
	device="/dev/`basename $__link`"
	this_partnum=`cat /sys/class/block/$devname/partition`
	is_last_part=0
fi

gpt_in_error=`check_gpt_for_error "Not all of the space available to"`
if [ $gpt_in_error -eq 1 ]
then
	echo "fix" | parted $device ---pretend-input-tty print \
			2>/dev/null 1>/dev/null
fi

gpt_in_error=`check_gpt_for_error "The backup GPT table is corrupt"`
if [ $gpt_in_error -eq 1 ]
then
	printf "ok\nfix\nfix\n" | parted $device ---pretend-input-tty print \
			2>/dev/null 1>/dev/null
fi

#Get the last partition's number and fstype.

fstype=`parted -s -m  $device print | tail -n1 |cut -f5 -d':'`
partnum=`parted -s -m $device  print | tail -n1 |cut -f1 -d':'`
__freespace=`parted -s -m $device unit MiB print free | tail -n1 |grep free`
if [ "$__freespace"x = x ]
then
	echo "Device $device has no free space. Exiting"
	exit -1
fi

#Check if the provided device is the last partition.

if [ ${is_last_part} -eq 0 ]
then
	if [ $partnum -ne $this_partnum ]
	then
		echo "Provided device $realdevice is not last partition. Exiting"
		exit -1
	fi
fi

freespace=`echo ${__freespace}| tail -n1 |cut -f4 -d':'|cut -f1 -d'M'`
if [ `echo $fstype | grep ext | wc -l` -gt 0 ]
then
cat <<EOF
	Using device $device
	Found filesystem type $fstype.
	Found partition $partnum on device $device to resize
	Found freespace $freespace MiB
	Attempting to extend partition $partnum of device $device with $freespace MiB
EOF

#Extend partition size
parted -s -- $device unit % resizepart $partnum 100%
if [ $? -ne 0 ]
then
	echo "Couldn't extend partition $partnum of device $device"
	exit -1
fi

#Extend file system
if [ "$device" != "$realdevice" ]
then
	resize2fs $realdevice
	[ $? -ne 0 ] && echo "Couldn't extend filesystem on $realdevice" && exit -1
	e2fsck -f -y -C 0 $realdevice
else
	resize2fs ${device}${partnum}
	[ $? -ne 0 ] && echo "Couldn't extend filesystem on $partnum of device $device" && exit -1
	e2fsck -f -y -C 0 ${device}${partnum}
fi

#If an ext filesystem wasn't found.
else
	echo "Last partition is not an ext filesystem. Not resizing"
	exit -1
fi
exit 0
