#!/bin/sh

set -x;
set -e;
dir="$1"
arch="${ARCH}"
cpio_file=$(mktemp /tmp/magiccpio.XXXXXXX)
if [ "$dir"x = x ]
then
	dir=$(pwd)
fi
[ ! -d $dir/${SUBDIR} ] && echo "Directory $dir/${SUBDIR} not found" && exit -1

cd $dir/${SUBDIR}; 
[ $? -ne 0 ] && echo "Can not change directory to $dir/${SUBDIR}" && exit -1

ls -la; find .  | cpio -ov -H newc > $cpio_file
#xz -k $cpio_file
gzip -c $cpio_file > $cpio_file.xz
fl=$(stat -c %s $cpio_file.xz)
rem=$(($fl % 4))
fb= ;
if [ $rem = 0 ]; then
  fb=$fl
else
  fb=$(($fl + 4 - $rem))
fi;
dd if=/dev/zero of=$cpio_file.xz4 bs=1 count=$fb
dd if=$cpio_file.xz of=$cpio_file.xz4 conv=notrunc
mv $cpio_file.xz4 $OLDPWD/addon.cpio.${arch}.gz4
cd -;
